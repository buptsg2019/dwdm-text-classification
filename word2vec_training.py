from keras.preprocessing import text
from gensim.models.word2vec import Word2Vec

from dataset_preperation import *


train, train_x, valid_x, train_y, valid_y = load_dataset()

token = text.Tokenizer()
token.fit_on_texts(train['text'])

texts = []
for i in range(train['text'].size):
    texts.append(text.text_to_word_sequence(train['text'][i]))

word2vec_model = Word2Vec(sentences=texts, vector_size=300, min_count=1)
word2vec_model.save('data/word2vec.model')

word2vec_model = Word2Vec.load('data/word2vec.model')
print(word2vec_model.wv['this'])
