import torch
from matplotlib import pyplot as plt
from sklearn.metrics import recall_score, precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import roc_curve, auc


def auc_score(y_true, y_score):
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    roc_auc = auc(fpr, tpr)
    plt.figure()
    plt.plot(fpr, tpr, color='darkorange', lw=2, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([-0.1, 1.05])
    plt.ylim([-0.1, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()
    return roc_auc


def get_score(y_pre_raw, y_true):
    y_pre_raw = torch.Tensor(y_pre_raw)
    y_pre = (y_pre_raw > 0.5).tolist()
    y_pre_raw = y_pre_raw.tolist()
    v = (y_pre == y_true).sum() / len(y_true)
    print("Accuracy: " + str(v))
    p = precision_score(y_true, y_pre)
    print("Precision_score: " + str(p))
    r = recall_score(y_true, y_pre)
    print("Recall score: " + str(r))
    f1 = f1_score(y_true, y_pre)
    print("F1 score: " + str(f1))
    roc = auc_score(y_true, y_pre_raw)
    print("Roc score: " + str(roc))
