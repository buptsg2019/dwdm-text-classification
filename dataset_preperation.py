from sklearn import model_selection, preprocessing
import pandas


def load_dataset():
    data = open('data/corpus').read()
    labels, texts = [], []
    for i, line in enumerate(data.split("\n")):
        content = line.split()
        labels.append(content[0])
        texts.append(" ".join(content[1:]))

    trainDF = pandas.DataFrame()
    trainDF['text'] = texts
    trainDF['label'] = labels

    train_x, valid_x, train_y, valid_y = model_selection.train_test_split(trainDF['text'], trainDF['label'],
                                                                          train_size=0.8, random_state=42)

    encoder = preprocessing.LabelEncoder()
    train_y = encoder.fit_transform(train_y)
    valid_y = encoder.fit_transform(valid_y)

    return trainDF, train_x, valid_x, train_y, valid_y
