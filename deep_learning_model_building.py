# 深度学习模型
# 1 RNN
# 2 LSTM

import time
import math
import random
import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.optim import lr_scheduler

import matplotlib.pyplot as plt


def asMinutes(s):
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


def timeSince(since, percent):
    now = time.time()
    s = now - since
    es = s / (percent)
    rs = es - s
    return '%s (- %s)' % (asMinutes(s), asMinutes(rs))


def showPlot(points):
    plt.figure()
    fig, ax = plt.subplots()
    plt.plot(points)


def train(model, input_tensor, target_tensor, optimizer, criterion):
    optimizer.zero_grad()
    target_length = target_tensor.size(0)
    outputs = model(input_tensor)
    loss = criterion(outputs, target_tensor)
    loss.backward()
    optimizer.step()
    return loss.item() * 100


def trainIters(model, train_input, train_label, n_iters, batch_size=500, print_every=1000, plot_every=500,
               learning_rate=0.001):
    model.train()
    assert n_iters % batch_size == 0
    assert print_every % batch_size == 0
    assert plot_every % batch_size == 0
    start = time.time()
    plot_losses = []
    print_loss_total = 0
    plot_loss_total = 0
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=4, gamma=0.5)
    criterion = nn.NLLLoss()

    for iter in range(batch_size, n_iters + 1, batch_size):
        indexx = [random.randint(0, len(train_label) - 1) for x in range(batch_size)]
        input_tensor = train_input[indexx]
        target_tensor = train_label[indexx]

        loss = train(model, input_tensor, target_tensor, optimizer, criterion)
        print_loss_total += loss
        plot_loss_total += loss

        if iter % print_every == 0:
            exp_lr_scheduler.step()
            print_loss_avg = print_loss_total / print_every
            print_loss_total = 0
            print('%s (%d %d%%) %.4f' % (timeSince(start, iter / n_iters), iter, iter / n_iters * 100, print_loss_avg))

        if iter % plot_every == 0:
            plot_loss_avg = plot_loss_total / plot_every
            plot_losses.append(plot_loss_avg)
            plot_loss_total = 0

    showPlot(plot_losses)


def predict(model, train_input):
    model.eval()
    # _, outputs = model(train_input).max(dim=1)
    outputs = torch.cat([model(train_input[:1000])[:, 1], model(train_input[1000:])[:, 1]])
    outputs = torch.exp(outputs)
    return outputs


class Rnn(nn.Module):
    def __init__(self, input_size, hidden_size=512):
        super(Rnn, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size

        self.rnn = nn.RNN(input_size, hidden_size, batch_first=True)
        self.out_linear = nn.Linear(hidden_size, 2)
        self.dropout = nn.Dropout(p=0.2)

    def forward(self, input):
        X, _ = self.rnn(input)
        output = self.out_linear(X.sum(dim=1))
        return F.log_softmax(self.dropout(output), dim=-1)


class Lstm(nn.Module):
    def __init__(self, input_size, hidden_size=512):
        super(Lstm, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size

        self.lstm = nn.LSTM(input_size, hidden_size, batch_first=True)
        self.out_linear = nn.Linear(hidden_size, 2)
        self.dropout = nn.Dropout(p=0.2)

    def forward(self, input):
        X, _ = self.lstm(input)
        output = self.out_linear(X.sum(dim=1))
        return F.log_softmax(self.dropout(output), dim=-1)


def rnn(train_x2, train_y2, valid_x2):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # device = 'cpu'
    # train_x3 = torch.tensor(train_x2).to(device)
    # train_y3 = torch.tensor(train_y2).to(device)
    # valid_x3 = torch.tensor(valid_x2).to(device)
    train_x3 = train_x2.to(device)
    train_y3 = train_y2.to(device)
    valid_x3 = valid_x2.to(device)
    model = Rnn(train_x3.size(-1)).to(device)
    print("begin to train")
    trainIters(model, train_x3, train_y3, 150000, print_every=10000, plot_every=1000)
    torch.save(model.state_dict(), './data/rnn_modelpth')
    return predict(model, valid_x3).tolist()


def lstm(train_x2, train_y2, valid_x2):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # device = 'cpu'
    # train_x3 = torch.tensor(train_x2).to(device)
    # train_y3 = torch.tensor(train_y2).to(device)
    # valid_x3 = torch.tensor(valid_x2).to(device)
    train_x3 = train_x2.to(device)
    train_y3 = train_y2.to(device)
    valid_x3 = valid_x2.to(device)
    model = Lstm(train_x3.size(-1)).to(device)
    print("begin to train")
    trainIters(model, train_x3, train_y3, 150000, print_every=10000, plot_every=1000)
    torch.save(model.state_dict(), './data/lstm_modelpth')
    return predict(model, valid_x3).tolist()
