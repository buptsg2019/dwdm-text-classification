# 传统机器学习模型
# 1 Bayesian
# 2 决策树
# 3 SVM
# 4 XGBoost

from sklearn import naive_bayes, tree, svm
from sklearn.model_selection import GridSearchCV
from xgboost import XGBClassifier
import pickle

from dataset_preperation import *
from feature_engineering import *
from evaluate_score import *


class Bayesian:
    def __init__(self, train_x, train_y, valid_x):
        self.train_x = train_x
        self.train_y = train_y
        self.valid_x = valid_x
        self.filename = 'data/bayesian.pkl'

    def train(self):
        model = naive_bayes.MultinomialNB()
        model.fit(self.train_x, self.train_y)
        with open(self.filename, 'wb') as file:
            pickle.dump(model, file)

    def predict(self):
        with open(self.filename, 'rb') as file:
            model = pickle.load(file)
        pred_y = model.predict(self.valid_x)
        return pred_y


class DecisionTree:
    def __init__(self, train_x, train_y, valid_x):
        self.train_x = train_x
        self.train_y = train_y
        self.valid_x = valid_x
        self.filename = 'data/decision_tree.pkl'

    def train(self):
        model = tree.DecisionTreeClassifier(criterion='entropy')
        model.fit(self.train_x, self.train_y)
        with open(self.filename, 'wb') as file:
            pickle.dump(model, file)

    def predict(self):
        with open(self.filename, 'rb') as file:
            model = pickle.load(file)
        pred_y = model.predict(self.valid_x)
        return pred_y


class SVM:
    def __init__(self, train_x, train_y, valid_x):
        self.train_x = train_x
        self.train_y = train_y
        self.valid_x = valid_x
        self.filename = 'data/svm.pkl'

    def train(self):
        model = svm.SVC(gamma='auto')
        model.fit(self.train_x, self.train_y)
        with open(self.filename, 'wb') as file:
            pickle.dump(model, file)

    def predict(self):
        with open(self.filename, 'rb') as file:
            model = pickle.load(file)
        pred_y = model.predict(self.valid_x)
        return pred_y


class XGBoost:
    def __init__(self, train_x, train_y, valid_x):
        self.train_x = train_x
        self.train_y = train_y
        self.valid_x = valid_x
        self.filename = 'data/xgboost.pkl'

    def train(self):
        model = XGBClassifier(max_depth=5, min_child_weight=3,
                              learning_rate=0.3, n_estimators=200)
        model.fit(self.train_x, self.train_y)
        with open(self.filename, 'wb') as file:
            pickle.dump(model, file)

    def predict(self):
        with open(self.filename, 'rb') as file:
            model = pickle.load(file)
        pred_y = model.predict(self.valid_x)
        return pred_y
