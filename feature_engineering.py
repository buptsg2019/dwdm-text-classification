from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from keras.preprocessing import text, sequence
from gensim.models.word2vec import Word2Vec
import numpy as np


def bow(train, train_x, valid_x):
    count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}')
    count_vect.fit(train['text'])

    train_bow = count_vect.transform(train_x)
    valid_bow = count_vect.transform(valid_x)
    return train_bow, valid_bow


def tfidf(train, train_x, valid_x, mode="word"):
    if mode == "word":
        # 词语级tf-idf
        tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000)
        tfidf_vect.fit(train['text'])
        train_tfidf = tfidf_vect.transform(train_x)
        valid_tfidf = tfidf_vect.transform(valid_x)
    elif mode == "ngram":
        # ngram 级tf-idf
        tfidf_vect_ngram = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
                                           max_features=5000)
        tfidf_vect_ngram.fit(train['text'])
        train_tfidf = tfidf_vect_ngram.transform(train_x)
        valid_tfidf = tfidf_vect_ngram.transform(valid_x)
    else:
        # 词性级tf-idf
        tfidf_vect_ngram_chars = TfidfVectorizer(analyzer='char', token_pattern=r'\w{1,}', ngram_range=(2, 3),
                                                 max_features=5000)
        tfidf_vect_ngram_chars.fit(train['text'])
        train_tfidf = tfidf_vect_ngram_chars.transform(train_x)
        valid_tfidf = tfidf_vect_ngram_chars.transform(valid_x)
    return train_tfidf, valid_tfidf


def fasttext(train, train_x, valid_x):
    # 加载预训练模型词向量
    embeddings_index = {}
    for i, line in enumerate(open('data/wiki-news-300d-1M.vec', 'rb')):
        values = line.split()
        embeddings_index[values[0]] = np.asarray(values[1:], dtype='float32')

    # 创建分词器
    token = text.Tokenizer()
    token.fit_on_texts(train['text'])
    word_index = token.word_index
    # 1 ~ 2587

    # 将文本转化为分词序列，并填充使向量长度相等
    train_seq_x = sequence.pad_sequences(token.texts_to_sequences(train_x), maxlen=100)
    valid_seq_x = sequence.pad_sequences(token.texts_to_sequences(valid_x), maxlen=100)

    # 创建分词到嵌入向量的映射
    embedding_matrix = np.zeros((len(word_index) + 1, 300))
    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

    train_word2vec = np.zeros(train_seq_x.shape + (300,))
    valid_word2vec = np.zeros(valid_seq_x.shape + (300,))

    train_d, train_w = train_seq_x.shape
    valid_d, valid_w = valid_seq_x.shape

    for i in range(train_d):
        for j in range(train_w):
            train_word2vec[i][j] = embedding_matrix[train_seq_x[i][j]]
    for i in range(valid_d):
        for j in range(valid_w):
            valid_word2vec[i][j] = embedding_matrix[valid_seq_x[i][j]]
    return train_word2vec, valid_word2vec


def word2vec(x):
    word2vec_model = Word2Vec.load('data/word2vec.model')

    x_list = list(x)

    x_text = []
    for i in range(len(x_list)):
        x_text.append(text.text_to_word_sequence(x_list[i]))

    x_word2vec = np.zeros((x.size, 200, 300))
    for i in range(x.size):
        for j in range(200):
            if j < len(x_text[i]):
                x_word2vec[i][j] = word2vec_model.wv[x_text[i][j]]

    return x_word2vec
